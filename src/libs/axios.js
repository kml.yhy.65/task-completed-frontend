import axios from 'axios'
const token = localStorage.getItem('token_my_app')
const axiosIns = axios.create({
    baseURL: 'http://127.0.0.1:8000/api',
    headers: {
        Authorization: `Bearer ${token}`,
        ContentType: "multipart/form-data;"
    },
})

export default axiosIns