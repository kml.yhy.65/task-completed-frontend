import { createRouter, createWebHistory } from 'vue-router'
import Home from '../views/pages/Home.vue'
import { useAuthStore } from '../stores/auth'
import i18n from '../libs/i18n'
const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: '/',
      name: 'home',
      component: Home,
      meta: {
        layout: false,
        permission: ['Super Admin', 'User']
      }
    },
    {
      path: '/users',
      name: 'users',
      component: () => import('../views/pages/Users.vue'),
      meta: {
        layout: false,
        permission: ['Super Admin']

      },

    },
    {
      path: '/newUser/:id?',
      name: 'newUser',
      component: () => import('../views/pages/NewUser.vue'),
      props: true,
      meta: {
        layout: false,
        permission: ['Super Admin', 'User']
      }
    }
    ,
    {
      path: '/report',
      name: 'reports',
      component: () => import('../views/pages/Reports.vue'),
      meta: {
        layout: false,
        permission: ['Super Admin']

      },

    },
    {
      path: '/login',
      name: 'login',
      component: () => import('../views/login/Login.vue'),
      meta: {
        layout: true,
      }
    }
  ]
})
router.beforeEach((to, from, next) => {
  const { token, isLogin, role } = useAuthStore();
  const userById = localStorage.getItem('id_my_app');
  if (to.name !== 'login' && !isLogin && !token) {
    next({ name: 'login' })
  } else {
    if (role === 'User'&&to.name !== 'login') {
      var findPermission= [...to.meta.permission]
      var permission=userById==to.params.id?'true':'false'
      if (findPermission.includes('User')) {
        return next();
      } else {
        alertify.error(i18n.global.t('accessMessage'));
        return router.push({ name: 'home' });
      }
    } else {
      return next();
    }
    return next();

  }
})
export default router
