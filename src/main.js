import { createApp } from 'vue'
import { createPinia } from 'pinia'



import './assets/main.css'

import App from './App.vue'
import router from './router'
import axiosInstance from './libs/axios.js'
import i18n from './libs/i18n'
import VueSweetalert2 from 'vue-sweetalert2';
import 'sweetalert2/dist/sweetalert2.min.css';


const app = createApp(App)

app.use(createPinia())
app.use(router)
app.use(i18n)
app.use(VueSweetalert2);
app.config.globalProperties.$axios = { ...axiosInstance }
app.mount('#app')
