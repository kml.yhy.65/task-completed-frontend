import { defineStore } from 'pinia'

import router from '../router';
import axiosIns from "../libs/axios.js";

export const useAuthStore = defineStore({
    id: 'auth',

    state: () => ({
        token: localStorage.getItem('token_my_app'),
        isLogin: false,
        loading: false,
        role: localStorage.getItem('r3l_my_app'),
        userId: localStorage.getItem('id_my_app'),
    }),
    getters: {
        getUrlKeyword: (state) => {
            return state.urlKeyword;
        }
    }
    , actions: {
        async login(username, password) {
            this.loading = true
            const token = await axiosIns.post(`/login`, {
                "email": username,
                "password": password
            }).then(async res => {
                if (res.status == 200) {
                    this.token = res.data.token;
                    this.role = res.data.role[0]
                    this.isLogin = true;
                    axiosIns.defaults.headers.Authorization = `Bearer ${res.data.token}`
                    localStorage.setItem('token_my_app', res.data.token)
                    localStorage.setItem('r3l_my_app', res.data.role[0])
                    this.loading = false
                    if (this.role == "User") {
                        await axiosIns.get('/profile-details').then(res => {
                            console.log(res);
                            this.userId = res.data.id
                            localStorage.setItem('id_my_app', res.data.id)
                            alertify.success('Giriş Başarılı Yönlendiriliyorsunuz')
                            router.push({ name: 'newUser', params: { id: res.data.id } })
                        }).catch(err => {
                            console.log(err);
                        })
                    } else {
                        alertify.success('Giriş Başarılı Yönlendiriliyorsunuz')
                        router.push({ name: 'home' })
                    }
                } else {
                    console.log(res);
                    this.loading = false
                }
            }).catch((err) => {
                this.loading = false
                console.log(err);
                alertify.error(err.response.data.error)
            });
        },
        logout() {
            this.isLogin = false;
            this.token = null;
            this.role = null;
            this.userId = null;
            localStorage.removeItem('token_my_app')
            localStorage.removeItem('r3l_my_app')
            localStorage.removeItem('id_my_app')
            router.push({ name: 'login' })
        }
    }


});