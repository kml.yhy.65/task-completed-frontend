import axios from 'axios';
import { defineStore } from 'pinia'
import axiosIns from "../libs/axios.js";
import router from '../router';
import { useAuthStore } from './auth.js';
import i18n from '../libs/i18n'
const authStore = useAuthStore();

export const useUserStore = defineStore({
    id: 'user',
    state: () => ({
        userById: {},
        users: [],
        loading: false,
        error: {},
        searchText: null
    }),
    getters: {

    },
    actions: {
        async fetchUser() {
            this.loading = true
            const result = await axiosIns.get('/users/').
                then(res => {
                    this.users = res.data.users
                    this.loading = false
                }).catch(err => {
                    console.log(err)
                    alertify.error(err)
                })
        },
        async searchUser(name) {
            this.loading = true
            const result = await axiosIns.get('/searchUser/?q='+name).
                then(res => {
                    this.users = res.data.users
                    this.loading = false
                }).catch(err => {
                    console.log(err)
                    alertify.error(err)
                })
        },
        async addUser(payload) {
            this.loading = true

            await axiosIns.post('/users', {
                'name': payload.name,
                'email': payload.email,
                'password': payload.password,
                'role': payload.role
            }).then(res => {
                console.log(res);
                this.loading = false
                alertify.success(i18n.global.t('addUser'))
                router.push({ name: "users" });
            }).catch((err) => {
                this.error = err.response.data.error
                this.loading = false
                alertify.error(err)
            });
        },
        async deleteUser(payload, user) {
            axiosIns.delete('/users/' + payload).then(res => {
                this.users = this.users.filter(function (el) {
                    return el.id !== payload;
                });
                alertify.success(i18n.global.t('deleteUser'))
            }).catch(err => {
                console.log(err);
                alertify.error(err)
            })
        },
        async getUser(payload) {
            this.userById = {}
            this.loading = true
            this.error = {}
            this.search = null
            await axiosIns.get('/users/' + payload).then(res => {
                this.userById = res.data
                this.userById['role'] = res.data.roles[0].id
                this.loading = false
            }).catch((err) => {
                this.loading = false
                console.log(err);
                alertify.error(err)
            })
        },
        async updateUser(payload, id) {
            this.loading = true
            await axiosIns.patch('/users/' + id, payload).then(res => {
                this.loading = false
                alertify.success(i18n.global.t('updateUser'))
                router.push({ name: "users" });
            }).catch(err => {
                this.loading = false
                if(err.response){
                    this.error = err.response.data.error
                }else{
                    alertify.error(err)
                }

                console.log(err);
            })
        },
    }
})
