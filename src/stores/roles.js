import { defineStore } from 'pinia'
import axiosIns from "../libs/axios.js";
import { useAuthStore } from './auth.js';
const authStore = useAuthStore();

export const useRolesStore = defineStore({
    id: 'role',
    state: () => ({
        roles: [],
        loading: false,
    }),
    getters: {

    },
    actions: {
        async fetchRole() {
            this.loading = true
            await axiosIns.get('/roles').
                then(res => {
                    this.roles = res.data
                    this.loading = false
                }).catch(err => {
                    console.log(err)
                    alertify.error(err)
                })
        }
    }
})
