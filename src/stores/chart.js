import { defineStore } from 'pinia'
import axiosIns from "../libs/axios.js";

export const useChartStore = defineStore({
    id: 'chart',
    state: () => ({
        roleCount: [],
        loading:false,
        registerDateCount:[]
    }),
    getters: {

    },
    actions: {
        async fetchRoleCount() {
            this.loading = true
            await axiosIns.get('/countRole').
                then(res => {
                    this.roleCount = res.data
                    this.loading = false
                }).catch(err => {
                    console.log(err)
                    alertify.error(err)
                })
        },
        async fetchRegisterDateCount() {
            this.loading = true
            await axiosIns.get('/userRegisterCount').
                then(res => {
                    this.registerDateCount = res.data
                    this.loading = false
                }).catch(err => {
                    console.log(err)
                    alertify.error(err)
                })
        }
    }
})
